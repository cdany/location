/**
 */
import {RouterModule, Routes} from '@angular/router';
import {StoresComponent} from "./stores/stores.component";
import {GeoComponent} from "./geo/geo.component";


const APP_ROUTES:Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'},
  { path: '', component: GeoComponent },
  { path: 'geo', component: StoresComponent }

];

export const routing = RouterModule.forRoot(APP_ROUTES);
