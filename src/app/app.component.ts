import {Component, NgZone, ElementRef, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import { MapsAPILoader } from 'angular2-google-maps/core';
import {Router} from "@angular/router";


import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}

