import { Injectable } from '@angular/core';

@Injectable()
export class GeoService {
  form: any;
  stores: any[] = [
    {
          "storeId": 1,
          "storeName": "Rami Levy - Tel-Aviv",
          "geoLocation": {
            "lat": "31.084301",
            "long": "14.772136000000046"
    },
            "distance": 0
          },
        {
          "storeId": 2,
          "storeName": "Rami Levy - Herzliya",
          "geoLocation": {
            "lat": "42.772136000000046",
            "long": "24.772136000000046",
          },
          "distance" : 0

        },
        {
          "storeId": 3,
          "storeName": "Rami Levy - Eilat",
          "geoLocation": {
            "lat": "64.772136000000046",
            "long": "64.772136000000046",
    },
          "distance" : 0},

    {
      "storeId": 4,
      "storeName": "Shupersal - Tel-Aviv",
      "geoLocation": {
        "lat": "32.084301",
        "long": "34.772136000000046"
      },
      "distance": 0
    },
    {
      "storeId": 5,
      "storeName": "Shupersal - Ramat Hashron",
      "geoLocation": {
        "lat": "74.772136000000046",
        "long": "44.772136000000046",
      },
      "distance" : 0

    },
    {
      "storeId": 6 ,
      "storeName": "Shupersal - Eilat",
      "geoLocation": {
        "lat": "64.772136000000046",
        "long": "64.772136000000046",
      },
      "distance" : 0},

    {
      "storeId": 7,
      "storeName": "Mega - Tel-Aviv",
      "geoLocation": {
        "lat": "23.084301",
        "long": "94.772136000000046"
      },
      "distance": 0
    },
    {
      "storeId": 8,
      "storeName": "Mega - Herzliya",
      "geoLocation": {
        "lat": "49.772136000000046",
        "long": "49.772136000000046",
      },
      "distance" : 0

    },
    {
      "storeId": 9,
      "storeName": "Mega - Raanana",
      "geoLocation": {
        "lat": "12.772136000000046",
        "long": "64.772136000000046",
      },
      "distance" : 0},

    {
      "storeId": 10,
      "storeName": "Beitan - Tel-Aviv",
      "geoLocation": {
        "lat": "39.084301",
        "long": "34.772136000000046"
      },
      "distance": 0
    },
    {
      "storeId": 11,
      "storeName": "Beitan - Kfar Saba",
      "geoLocation": {
        "lat": "94.772136000000046",
        "long": "14.772136000000046",
      },
      "distance" : 0

    },
    {
      "storeId": 12,
      "storeName": "Beitan - Beer Sheva",
      "geoLocation": {
        "lat": "74.772136000000046",
        "long": "14.772136000000046",
      },
      "distance" : 0}
    ];

  constructor() {

    this.form = {
      lat: '',
      long: ''
    }
  }

  setForm(lat, long) {
    this.form.lat = lat;
    this.form.long = long;
    console.log(lat,long);
  }

  distance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
      c(lat1 * p) * c(lat2 * p) *
      (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }

  getAll(){
      for(let store of this.stores){
        store.distance = this.distance(this.form.lat, this.form.long, store.geoLocation.lat, store.geoLocation.long);
        console.log(store.distance);
      }
    return this.stores;
}
}
