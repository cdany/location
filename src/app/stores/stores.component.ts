import { Component, OnInit } from '@angular/core';
import {GeoService} from "../services/geo.service";


@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {
  stores: any[]=[];

  constructor(public GeoService:GeoService) {

  }

  ngOnInit() {

    this.stores = this.GeoService.getAll();


  }

}
