import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module

import { FormsModule} from '@angular/forms';
import { AgmCoreModule } from "angular2-google-maps/core";
import { StoresComponent } from './stores/stores.component';
import {routing} from "./app.routes";
import {GeoService} from "./services/geo.service";
import {MdAutocompleteModule, MdInputModule, MdButtonModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { GeoComponent } from './geo/geo.component';
import { OrderModule } from 'ngx-order-pipe';





@NgModule({
  declarations: [
    AppComponent,
    StoresComponent,
    GeoComponent
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyC-dmh5maqUhCjeK6N811ppUbzFdm0YsX0",
      libraries: ["places"]
    }),
    CommonModule,
    FormsModule,
    MdInputModule,
    ReactiveFormsModule,
    routing,
    OrderModule,
    MdButtonModule,
    BrowserAnimationsModule,
    MdAutocompleteModule,

  ],
  providers: [GeoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
